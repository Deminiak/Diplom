var myApp = angular.module('myApp', ['ngRoute','ngCookies']).
    config(['$routeProvider','$locationProvider',function ($routeProvider,$locationProvider) {
    $routeProvider.when('/home', {templateUrl: '/client/partials/home.html', controller: 'homeController'});
    $routeProvider.when('/secure',{templateUrl: '/client/partials/secure.html', controller: 'secureController'});
    $locationProvider.html5Mode({enabled: true, requireBase: false});
}]) ;

